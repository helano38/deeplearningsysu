from keras.layers import Dense, Flatten, Dropout
import keras.layers
import pickle
import numpy as np
import cv2
#importing Image data Generator
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from tensorflow.keras.preprocessing.image import array_to_img, img_to_array, load_img, save_img

datagen = ImageDataGenerator(
    # rotation_range=2
    #,
   #horizontal_flip=True
  zoom_range=[0.8,1.2]
    )

######Parametros#######################

image_size = 256
batch_size_train = 100
batch_size_validacao = 100
epochs_ = 2

train_dir = 'Test/'
validation_dir = 'dataset/down_sized_images/'

path_save = 'dataset/dataAugmentation/'


i = 0
for batch in datagen.flow_from_directory(train_dir, batch_size=1):
    img = array_to_img(batch[0][0])
    img2 = batch[0][0]
    save_img('dataset/dataAugmentation/zoom/'+str(i)+'.png', 
                img) 
    i += 1
    if i % 5 == 0:
        break