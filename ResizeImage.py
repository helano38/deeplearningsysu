
import numpy as np
from math import sqrt
from skimage import util
import cv2

def get_positive(number):
    if(number<0):
        return number*(-1)
    return number


def get_ROI(img, img2):

    x,y = img.shape

    ret,thresh = cv2.threshold(img,127,255,0)
    contours, hierarchy = cv2.findContours(thresh, 1, 2)

    cnt = contours[0]
    M = cv2.moments(cnt)
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])

    x,y,w,h = cv2.boundingRect(cnt)

    ROI = img2[y:y+h, x:x+w]

    return ROI


read_path= "dataset/corneaLabels/"
read_path_target ="dataset/segmented_images/"  
write_path = "dataset/segmented_image_croped/"
folder ="4/"
klass = "2/"


for i in range (676, 694):
    img = cv2.imread(read_path+str(i)+".png", 0)
    img2 = cv2.imread(read_path_target+str(i)+".jpg")
    
    ROI = get_ROI(img, img2)
    print(ROI.shape)

    cv2.imwrite(write_path+folder+klass+str(i)+".jpg", ROI)
