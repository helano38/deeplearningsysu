import tensorflow as tf
from keras import models
from keras import layers
from keras import optimizers
from keras import regularizers
from keras.applications import VGG16
from keras.applications import VGG19
from keras.applications.densenet import DenseNet121
from keras.applications import ResNet50
from keras.applications import Xception
from keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import roc_auc_score
from keras.models import load_model, Model
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense, Flatten, Dropout
import keras.layers
import pickle
import numpy as np



######Parametros#######################

image_size = 256
batch_size_train = 100
batch_size_validacao = 100
epochs_ = 2

train_dir = 'dataset/down_sized_images/'
validation_dir = 'dataset/down_sized_images/'

path_save = 'dataset/executed_saves/'

# get base model
base_model = VGG16(weights='imagenet', include_top=False,input_shape = (image_size, image_size, 3))

# build top model
x = Flatten(name='flatten')(base_model.output)
x = Dropout(0.5)(x)
x = Dense(1024, activation='relu', name='fc1')(x)
x = Dropout(0.5)(x)
#x = Dense(256, activation = 'relu', name='fc2')(x)
x = Dense(3, activation='softmax', name='predictions')(x)

# stitch together
model = Model(inputs= base_model.input, outputs=x)

# inspect
model.summary()


train_datagen = ImageDataGenerator(
    samplewise_center=True,
    samplewise_std_normalization=True
)

validation_datagen = ImageDataGenerator(
    samplewise_center=True,
    samplewise_std_normalization=True
)
    
train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(image_size, image_size),
        batch_size=batch_size_train,
        class_mode='categorical')
 
validation_generator = validation_datagen.flow_from_directory(
        validation_dir,
        target_size=(image_size, image_size),
        batch_size=batch_size_validacao,
        class_mode='categorical')#shuffle=False

# Compile the model
model.compile(loss='categorical_crossentropy',
              optimizer=optimizers.RMSprop(lr=0.00001),
              metrics=['acc'])

checkpoint = ModelCheckpoint('dataset/256_epochs_dropout_50_50_fine_{epoch:03d}.h5',period=1)

# Train the model
history = model.fit_generator(
      train_generator,
      steps_per_epoch= train_generator.samples/train_generator.batch_size ,
      epochs=epochs_,
      validation_data=validation_generator,
      validation_steps=validation_generator.samples/validation_generator.batch_size,
      callbacks=[checkpoint],
      verbose=1
      #class_weight=[0.25,0.75]
)
 
# Save the model
#model.save(path_save + model_name)

#Save pickle
#with open(path_save + 'historico_' + model_name[:-3] + '.pickle', 'wb') as f:
#    pickle.dump(history, f)