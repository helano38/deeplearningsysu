import cv2 as cv 
import numpy as np 
from skimage import io
from skimage import img_as_ubyte, img_as_float, img_as_int


raw_folder = "dataset/rawImages/"
cornea_folder = "dataset/corneaLabels/"
segmented_folder = "dataset/segmented_images/"

def create_new_matrix(img, window_size):
    x,y,z = img.shape
    x2 = x+(window_size*2)
    y2 = y+(window_size*2)
    matrix1 = np.zeros((x,y)) 
    matrix2 = np.zeros((x,y))
    matrix3 = np.zeros((x,y))


   # print(img.shape)
    #print(matrix.shape)
    for i in range(x):
        for j in range(y):
                matrix1[i][j]=img[i][j][0]
                matrix2[i][j]=img[i][j][1]
                matrix2[i][j]=img[i][j][2]
                
    matrix1 = np.pad(matrix1, ((window_size,window_size), (0,0)), mode='constant', constant_values=0)
    matrix2 = np.pad(matrix2,   ((window_size,window_size), (0,0)), mode='constant', constant_values=0)
    matrix3 = np.pad(matrix3, ((window_size,window_size), (0,0)), mode='constant', constant_values=0)

    # global_matrix = np.zeros((matrix1.shape[0], matrix1.shape[1], 3 ))
    global_matrix = np.zeros((matrix1.shape[0], matrix1.shape[1], 3))
#     teste = (window_size*2)+1
    for i in range(0, global_matrix.shape[0]):
         for j in range (0, global_matrix.shape[1]):
                 global_matrix[i][j][0]=matrix1[i][j]
                 global_matrix[i][j][1]=matrix2[i][j]
                 global_matrix[i][j][2]=matrix3[i][j] 
    #io.imsave('_new_matrix.png', img_as_float(global_matrix))

    #run_padding_image(img, global_matrix, window_size)

    return global_matrix
 

def create_new_matrix_two(img, window_size):
 
                
    matrix1 = np.pad(img, ((window_size,window_size), (0,0), (0,0)), mode='constant', constant_values=0)


    return matrix1
 

def segment_image(image, label, number):
    x,y,z = label.shape
    for i in range(x):
        for j in range (y):
            if (label[i][j][0] == 255 and label[i][j][1] == 255 and label[i][j][1] == 255):
                image[i][j][0]=255
                image[i][j][1]=255
                image[i][j][2]=255
    cv.imwrite(segmented_folder+str(number)+".jpg", image)        
                    


# image = cv.imread(raw_folder+str(1)+".jpg")
# print(image.shape)
# image2 = create_new_matrix(image, 432)
# print(image2.shape)
# cv.imwrite("IMAGE_PADDING.jpg", image2)

read_path = "dataset/segmented_images_with_padding/"
save_path = "dataset/k_fold_5_segmented_padding/"
folder = "4/"
klass = "2/"

for i in range (676, 694):
    image = io.imread(read_path+str(i)+".png")
    #image2 = create_new_matrix_two(img_as_int(image), 432)
    #image_u = img_as_float(image)
    io.imsave(save_path+folder+klass+str(i)+".jpg", image)
   
    
