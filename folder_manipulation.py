import os
import cv2
import shutil
import numpy as np
import random


read_path= "dataset/corneaLabels/"
read_path_target ="dataset/segmented_image_croped/"  
write_path = "dataset/blur_fold_5_dataset/"
folders = ["0", "1", "2", "3", "4"]
klasses = ["0", "1", "2"]


def get_median_blur(image):
    result = cv2.GaussianBlur(image,(31,31), 0)
    return result


for fold in folders:
    for klass in klasses:
    
        images = os.listdir(read_path_target+fold+"/"+klass+"/")
        for img_name in images:
            img = cv2.imread(read_path_target+fold+"/"+klass+"/"+img_name)
            #img2 = cv2.imread(read_path_target+img_name)
    
            ROI = get_median_blur(img)
            print(ROI.shape)
            print (write_path+fold+"/"+klass+"/"+img_name)
            cv2.imwrite(write_path+fold+"/"+klass+"/"+img_name, ROI)
