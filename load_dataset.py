
import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage import io
from skimage import util


def get_descritores(img, label):
    
    img_caminho = 'dataset/rawImages/'+img
    LARGURA = 649
    ALTURA = 432
    
    # Ler a imgem
    img_teste = cv2.imread(img_caminho)
    
    #Redimensionar
    img_redimensionada = cv2.resize(img_teste, (LARGURA, ALTURA), interpolation=cv2.INTER_CUBIC)
    
    # Remover ruídos
    # img_equalizada = cv2.equalizeHist(img_redimensionada)
    # img_suavizada = cv2.GaussianBlur(img_equalizada, (9,9), 1)
    

    cv2.imwrite('dataset/croped_images/'+str(label)+'/'+img, img_redimensionada) 

    
    return img_redimensionada



def crop_images(img, label):
    image = io.imread('dataset/rawImages/'+img)

    B = util.crop(image, ((0, 0), (108, 109), (0,0)), copy=False)
    print(B.shape)
    io.imsave('dataset/croped_images/'+str(label)+'/'+img, B) 




def move_images(dir):
    df = pd.read_csv('dataset/information.csv')
    labels = df['Category'].values.tolist()
    file_names = df['file'].values.tolist()



def crop_images_from_folder(path, img):
    image= io.imread(path+img+'.jpg')

    croped = util.crop(image, ((0, 0), (108, 109), (0,0)), copy=False)

    io.imsave(path+img+'.jpg', croped) 




def imigrate_images():
    dir_0 = 'dataset/k_fold_10/0'
    dir_1 = 'dataset/k_fold_10/1'
    dir_2 = 'dataset/k_fold_10/2'
    dir_3 = 'dataset/k_fold_10/3'
    dir_4 = 'dataset/k_fold_10/4'
    dir_5 = 'dataset/k_fold_10/5'
    dir_6 = 'dataset/k_fold_10/6'
    dir_7 = 'dataset/k_fold_10/7'
    dir_8 = 'dataset/k_fold_10/8'
    dir_9 = 'dataset/k_fold_10/9'    
    
  

